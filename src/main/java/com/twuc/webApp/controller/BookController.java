package com.twuc.webApp.controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class BookController {

    @GetMapping(value = "/users/{userId}/books")
    public String findBook(@PathVariable(value = "userId") Integer userid) {
        return String.format("The book for user %s", userid);
    }

    @GetMapping(value = "/segments/good")
    public String getMethodNumber(){
        return "this is direct path result";
    }

    @GetMapping(value = "/segments/{books}")
    public String getMthodByBook(@PathVariable String books) {
        return String.format("this is result find by PathVariable %s", books);
    }

    @GetMapping(value = "/books")
    public String getBooksByBookId(@RequestParam Integer bookId) {
        return String.format("this is book %s", bookId);
    }

//    @GetMapping(value = "/books")
//    public String getBookByBookName(@RequestParam String bookName) {
//        return String.format("this is book %s", bookName);
//    }

    @GetMapping(value = "/books/effective")
    public String getEffectiveVersion(@RequestParam String version) {
        return String.format("this is effective %S", version);
    }
}
