package com.twuc.webApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class WildcardsController {

    @GetMapping(value = "/?/pen")
    public String getUserPen() {
        return "? can matching one letter";
    }

    @GetMapping(value = "/wildcards/*")
    public String getWildcardsInfo() {
        return "this is * wildcards";
    }

    @GetMapping(value = "/*/wildcards")
    public String getWildcardsResult() {
        return "this is * among the uri result";
    }

    @GetMapping(value = "/wildcards/before/*/after")
    public String getWildcardsResultBefore() {
        return "this is result given before/after request";
    }

    @GetMapping(value = "/*ld/info")
    public String getPrefixMatchResult() {
        return "this is result by prefix_matching";
    }

    @GetMapping(value = "/su*/info")
    public String getSuffixMatchResult() {
        return "this is result by suffix_matching";
    }

    @GetMapping(value = "/**/message")
    public String getMultinodeCardsMatchResult() {
        return "this is result by Multinode_card";
    }

    @GetMapping(value = "/regex/{name:[a-z]+}.{ext:[a-z]+}")
    public String getRegexMessage(@PathVariable String name, @PathVariable String ext){
        return String.format("this is %s.%s", name, ext);
    }


}
