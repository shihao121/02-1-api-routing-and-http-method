package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockRequestDispatcher;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class BookControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_book_message_given_books_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/45/books"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("The book for user 45"));
    }

    @Test
    void should_matcher_direct_path_given_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/segments/good"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("this is direct path result"));
    }

    @Test
    void should_return_bookmessage_given_findByBookId_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/books?bookId=1"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().string("this is book 1"));
    }

//    @Test
//    void should_return_bookmessage_given_finfByBookName_reques() throws Exception {
//        mockMvc.perform(MockMvcRequestBuilders.get("/api/books?bookName=effective-java"))
//                .andExpect(MockMvcResultMatchers.status().is(200))
//                .andExpect(MockMvcResultMatchers.content().string("this is book effective-java"));
//
//    }


    @Test
    void should_return_404_given_uri_without_uriParameters() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("api/books/effective"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }
}