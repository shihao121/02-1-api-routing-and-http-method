package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookControllerTest {

    private BookController bookController = new BookController();

    @Test
    void should_return_bookmessage_when_getBookBy_userid() {
        String book = bookController.findBook(23);
        assertEquals("The book for user 23", book);
    }

}