package com.twuc.webApp.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class WildcardsControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_404_when_path_contains_two_letters_pattern_question_mark() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/xi/pen"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void should_return_404_when_path_contains_no_letters_pattern_question_mark() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api//pen"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void should_return_200_message_when_path_contains_one_letters_pattern_question_mark() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/a/pen"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("? can matching one letter"));
    }

    @Test
    void should_return_200_message_given_one_words_match_wildcard() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wildcards/anything"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is * wildcards"));
    }

    @Test
    void should_return_200_message_given_one_words_match_wildcards_Among_the_uri() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/hello/wildcards"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is * among the uri result"));
    }

    @Test
    void should_return_404_given_before_after_request() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/before/after"))
                .andExpect(MockMvcResultMatchers.status().is4xxClientError());
    }

    @Test
    void should_return_200_message_given_request_match_Prefix_matching() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/wild/info"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is result by prefix_matching"));
    }

    @Test
    void should_return_200_message_given_request_match_suffix_matching() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/suffix/info"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is result by suffix_matching"));
    }

    @Test
    void should_return_200_message_given_request_match_Multinode_card_among_the_uri() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/firstNode/secondNode/message"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is result by Multinode_card"));
    }

    @Test
    void should_return_200_message_given_request_matching_regex() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/regex/image.jpg"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("this is image.jpg"));
    }
}